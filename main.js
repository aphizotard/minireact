import App from "./components/App.js";
import Didact from "./didact/Didact.js";


let root = document.getElementById("root");
let app = Didact.createElement(App, {});

Didact.render(app, root);
// Didact.render(
//     Didact.createElement('h1', {'class': 'toto titi tata', 'id': 'toto'},
//         Didact.createElement('h1', {}, 'Je suis un h1'),
//         Didact.createElement('h2', {}, 'Je suis un h2'),
//         Didact.createElement('button', {'class': 'btn btn-primary mt-3 ', 'type': 'submit'}, 'Valider')
//     ),
//     document.getElementById('root')
// );