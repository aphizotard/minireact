import Component from "../didact/Component.js";
import Didact from "../didact/didact.js";

export default class Button extends Component {

    constructor(props, children) {
        super(props, children);
    }


    render() {
        return Didact.createElement('button', this.props, this.children);
    }

}
