import Component from "../didact/Component.js";
import Button from "./Button.js"
import Didact from "../didact/didact.js";

class App extends Component {

    constructor(type, props, ...children) {
        super(type, props, children);
    }

    render() {
        // console.log(Didact.createElement(Button, {}, "hello world"));
        console.log("here");
        return Didact.createElement('div', {},
            Didact.createElement('h1', {}, 'je suis un h1'),
            Didact.createElement('h2', {}, 'je suis un h2'),
            //  Didact.createElement(Button, {}, 'je suis un button'),
        );
    }
}

export default App;


