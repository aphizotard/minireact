import Component from "./Component.js";

/**
 *
 * @param {*} type
 * @param {*} props
 * @param  {...any} children
 */
export function createElement(type, props, ...children){
    //vérifier le type
    //si c'est une fonction, type = 'function_element'
    return{
        type: typeof type == "function" ? "COMPONENT" : type,
        tag: typeof type == "function" ? type.name : type,
        component: typeof type == "function" ? new type() : null,
        props: {
            ...props,
            children: children.map(child => {
                return typeof child === 'object' ? child : creatTextElement(child)
            })
        }
    }
}

/**
 * Crée un noeud de type text
 * @param {any} text
 * @returns {{type: string, props: {nodeValue: *, children: []}}}
 */
function creatTextElement(text) {
    return {
        type: 'TEXT_ELEMENT',
        props: {
            nodeValue: text,
            children: []
        }
    }
}
/**
 * 
 * Rend l'élément en l'ajoutant au RenderDOM
 * @param {Object} element 
 * @param {HTMLElement} container 
 */
function render(element, container){
    //si type function_element, appelle la fonction render du composant
    let dom;
    if(element.type == "TEXT_ELEMENT"){
        dom = container;
        dom.appendChild(document.createTextNode(element.props.nodeValue));
    }   
    else if(element.type == "COMPONENT"){
        let component = element.component.render();
        dom = render(component, container)
        console.log(dom)
    }
    else{
        dom = document.createElement(element.type);
    }
    if(element.props) {
        Object.keys(element.props).forEach(name => {
            if (name !== 'children' ) {
                if(name === 'class') {
                    const splitted = element.props[name].split(' ');
                    splitted.forEach(x => {
                        dom.classList.add(x)
                    })
                } else {
                    dom[name] = element.props[name];
                }
            }

        });
    }

    if (element.props.children) {
        element.props.children.forEach(child => {
            render(child, dom)
        });
    }


    if(element.type !== "TEXT_ELEMENT"){
        return container.appendChild(dom)
    }
}

const Didact = {
    createElement,
    render
};

export default Didact