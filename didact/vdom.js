// /**
//  * 
//  * @param {*} type 
//  * @param {*} props 
//  * @param  {...any} children 
//  */
// export function createElement(type, props, ...children){
//     return{
//         type,
//         props: {
//             ...props,
//             children: children.map(child => {
//                return typeof child === 'object' ? child : creatTextElement(child)
//             })
//         }
//     }
// }

// /**
//  * Crée un noeud de type text
//  * @param {any} text
//  * @returns {{type: string, props: {nodeValue: *, children: []}}}
//  */
// function creatTextElement(text){
//     return {
//         type: 'TEXT_ELEMENT',
//         props: {
//             nodeValue: text,
//             children: []
//         }
//     }
// }

// const Didact = {
//     createElement
// };

// export default Didact;