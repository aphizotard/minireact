class Component{

    props = null;

    constructor(type, props, ...children) {
        this.props = props;
        this.type = type;
        this.children =children;
    }

    /*setState(newState) {
        this.state = { ...this.state, ...newState };
        document.dispatchEvent(new Event("rerender"));
    }*/

    //display(){

    //}


    render() {
        throw "Must implement render function";
    }

}
export default Component;