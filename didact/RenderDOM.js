class RenderDOM {

    constructor(component, container) {
        this.component = component;
        this.container = container;
        this.renderDOM();
    }

    renderDOM() {
        //this.container.appendChild(this.component.render());
        this.container.appendChild(this.component);
    }

}
export default RenderDOM;